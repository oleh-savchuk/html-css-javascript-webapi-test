

var month = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

//  Rounded & Shadowed Alerts
//  http://bootdey.com/snippets/view/Rounded-and-Shadowed-Alerts

//  http://www.chartjs.org/docs/#getting-started-installation
var wAPIkey = 'd594091fef73198a33e8e4ef863f250b';
var wCityCodes = [Kyiv => 703448, Lviv => 702550, Odesa => 698740]

// weather API request: 
// http://api.openweathermap.org/data/2.5/forecast?q=Kiev,ua&units=metric&appid=d594091fef73198a33e8e4ef863f250b&cnt=10

tempArr = Array();
function reqListener () {

    wData = JSON.parse(this.responseText);
    console.log(wData);

    document.getElementById("condidions").innerHTML = "<h4>WEATHER CONDITIONS</h4><div></div>";
    document.getElementById("humidity").innerHTML = "<h4>HUMIDITY</h4><div></div>";
    document.getElementById("windspeed").innerHTML = "<h4>WIND SPEED</h4><div></div>";

    // setting chart labels and additional data 
    tempArr = Array();
    for (i = 0; i < wData.list.length; i++) {
        if (i<11) {
            var thisdate = new Date(wData.list[i].dt_txt); 
            thisdate.getMinutes()<10 ? mymins="0"+thisdate.getMinutes() : mymins=thisdate.getMinutes();
            myDate=month[thisdate.getMonth()]+" "+thisdate.getDate()+", "+thisdate.getHours()+":"+mymins;
            tempArr.push (myDate);
            document.getElementById("condidions").innerHTML += "<div>"+myDate+" <span>"+capitalizeFirstLetter(wData.list[i].weather[0].description)+"</span></div>";
            document.getElementById("humidity").innerHTML += "<div>"+myDate+" <span>"+wData.list[i].main.humidity+"%</span></div>";
            document.getElementById("windspeed").innerHTML += "<div>"+myDate+" <span>"+wData.list[i].wind.speed+"m/s</span></div>";
        }
    }
    myChart.data.labels=tempArr;

    // setting chart data 
    var tempArr = Array();
    for (i = 0; i < wData.list.length; i++) {
        if (i<11) {
            tempArr.push(parseInt(wData.list[i].main.temp.toFixed(0)));
        }
    }
    myChart.data.datasets[0].data=tempArr;

    myChart.update()
    console.log('tempArr:'+tempArr);
    console.log('myChart.data.datasets[0].data:'+myChart.data.datasets[0].data);
}

function updateWeather (city) {
    var oReq = new XMLHttpRequest();
    oReq.addEventListener("load", reqListener);
    oReq.open("GET", "http://api.openweathermap.org/data/2.5/forecast?q="+city+",ua&units=metric&appid=d594091fef73198a33e8e4ef863f250b&cnt=10");
    oReq.send();
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

updateWeather('kiev');
